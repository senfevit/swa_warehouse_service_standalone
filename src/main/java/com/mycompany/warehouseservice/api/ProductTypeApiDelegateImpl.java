package com.mycompany.warehouseservice.api;

import com.mycompany.warehouseservice.ProductType;
import com.mycompany.warehouseservice.ProductTypeMapper;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mycompany.warehouseservice.ProductTypeRepository;
import com.mycompany.warehouseservice.api.model.ProductTypeDto;
import com.mycompany.warehouseservice.api.ProductTypeApiDelegate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Optional;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.badRequest;


@Service
public class ProductTypeApiDelegateImpl implements ProductTypeApiDelegate{

    private final ProductTypeRepository productTypeRepository;

    public ProductTypeApiDelegateImpl(ProductTypeRepository productTypeRepository) {
        this.productTypeRepository = productTypeRepository;
    }

    @Override
    public ResponseEntity<Void> addProductType(ProductTypeDto productTypeDto) {
        try {
            ProductType productType = ProductTypeMapper.INSTANCE.mapTo(productTypeDto);
            this.productTypeRepository.save(productType);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
        }

    }

    @Override
    public ResponseEntity<ProductTypeDto> getProductTypeById(
            @Parameter(name = "ProductTypeId", description = "ID of ProductType to return",
                    required = true, schema = @Schema(description = ""))
            @PathVariable("ProductTypeId") Long productTypeId) {

        // Convert Dto to warehouseProduct
        Optional<ProductType> productType = this.productTypeRepository.findById(productTypeId);
        return productType.map(value -> ResponseEntity.ok(ProductTypeMapper.INSTANCE.mapTo(value))).orElseGet(() ->
                new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @Override
    public ResponseEntity<List<ProductTypeDto>> productTypeGet() {
        return ResponseEntity.ok(
                this.productTypeRepository.findAll()
                        .stream().map(ProductTypeMapper.INSTANCE::mapTo)
                        .collect(Collectors.toList()));
    }

    @Override
    public ResponseEntity<Void> updateProductType(Long productTypeId, ProductTypeDto productTypeDto) {
        Optional<ProductType> productType = this.productTypeRepository.findById(productTypeId);
        if (productType.isPresent()) {
            productTypeRepository.save(ProductTypeMapper.INSTANCE.mapTo(productTypeDto));
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}