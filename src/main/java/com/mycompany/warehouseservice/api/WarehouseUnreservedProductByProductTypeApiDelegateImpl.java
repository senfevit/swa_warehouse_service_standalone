package com.mycompany.warehouseservice.api;

import com.mycompany.warehouseservice.WarehouseProduct;
import com.mycompany.warehouseservice.WarehouseProductMapper;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mycompany.warehouseservice.WarehouseProductRepository;
import com.mycompany.warehouseservice.api.model.WarehouseProductDto;
import com.mycompany.warehouseservice.api.WarehouseUnreservedProductByProductTypeApiDelegate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Optional;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.badRequest;


@Service
public class WarehouseUnreservedProductByProductTypeApiDelegateImpl implements WarehouseUnreservedProductByProductTypeApiDelegate{

    private final WarehouseProductRepository warehouseProductRepository;

    public WarehouseUnreservedProductByProductTypeApiDelegateImpl(WarehouseProductRepository warehouseProductRepository) {
        this.warehouseProductRepository = warehouseProductRepository;
    }


    @Override
    public ResponseEntity<List<WarehouseProductDto>> getWarehouseUnreservedProductByProductType(
            @Parameter(name = "productTypeId", description = "ID of productTypeId used in returned WarehouseProducts",
                    required = true, schema = @Schema(description = ""))
            @PathVariable("productTypeId") Long productTypeId) {
        return ResponseEntity.ok(
                warehouseProductRepository.findUnreservedByProductTypeId(productTypeId)
                        .stream().map(WarehouseProductMapper.INSTANCE::mapTo)
                        .collect(Collectors.toList()));
    }
}