package com.mycompany.warehouseservice.api;

import com.mycompany.warehouseservice.WarehouseProduct;
import com.mycompany.warehouseservice.WarehouseProductMapper;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mycompany.warehouseservice.WarehouseProductRepository;
import com.mycompany.warehouseservice.api.model.WarehouseProductDto;
import com.mycompany.warehouseservice.api.WarehouseProductApiDelegate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Optional;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.badRequest;


@Service
public class WarehouseProductApiDelegateImpl implements WarehouseProductApiDelegate{

    private final WarehouseProductRepository warehouseProductRepository;

    public WarehouseProductApiDelegateImpl(WarehouseProductRepository warehouseProductRepository) {
        this.warehouseProductRepository = warehouseProductRepository;
    }


    @Override
    public ResponseEntity<Void> addWarehouseProduct(WarehouseProductDto warehouseProductDto) {
        try {
            WarehouseProduct warehouseProduct = WarehouseProductMapper.INSTANCE.mapTo(warehouseProductDto);
            this.warehouseProductRepository.save(warehouseProduct);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
        }

    }

    @Override
    public ResponseEntity<WarehouseProductDto> getWarehouseProductById(
            @Parameter(name = "WarehouseProductId", description = "ID of WarehouseProduct to return",
                    required = true, schema = @Schema(description = ""))
            @PathVariable("WarehouseProductId") Long warehouseProductId) {

        // Convert Dto to warehouseProduct
        Optional<WarehouseProduct> warehouseProduct = this.warehouseProductRepository.findById(warehouseProductId);
        return warehouseProduct.map(value -> ResponseEntity.ok(WarehouseProductMapper.INSTANCE.mapTo(value))).orElseGet(() ->
                new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }


    @Override
    public ResponseEntity<List<WarehouseProductDto>> warehouseProductGet() {
        return ResponseEntity.ok(
                this.warehouseProductRepository.findAll()
                        .stream().map(WarehouseProductMapper.INSTANCE::mapTo)
                        .collect(Collectors.toList()));
    }

    @Override
    public ResponseEntity<Void> deleteWarehouseProduct(Long warehouseProductId, String apiKey) {
        Optional<WarehouseProduct> warehouseProduct = this.warehouseProductRepository.findById(warehouseProductId);
        if (warehouseProduct.isPresent()) {
            warehouseProductRepository.deleteById(warehouseProductId);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<Void> updateWarehouseProduct(Long warehouseProductId, WarehouseProductDto warehouseProductDto) {
        Optional<WarehouseProduct> warehouseProduct = this.warehouseProductRepository.findById(warehouseProductId);
        if (warehouseProduct.isPresent()) {
            warehouseProductRepository.save(WarehouseProductMapper.INSTANCE.mapTo(warehouseProductDto));
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}