package com.mycompany.warehouseservice;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.mycompany.warehouseservice.api.model.WarehouseProductDto;

@Mapper
public interface WarehouseProductMapper {
    public static WarehouseProductMapper INSTANCE = Mappers.getMapper(WarehouseProductMapper.class);

    WarehouseProductDto mapTo(WarehouseProduct wp);
    WarehouseProduct mapTo(WarehouseProductDto wpDto);
}