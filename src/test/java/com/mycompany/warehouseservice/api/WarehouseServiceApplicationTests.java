package com.mycompany.warehouseservice.api;

import com.mycompany.warehouseservice.WarehouseProduct;
import com.mycompany.warehouseservice.WarehouseService;
import com.mycompany.warehouseservice.WarehouseProductMapper;
import com.mycompany.warehouseservice.api.model.WarehouseProductDto;

import com.mycompany.warehouseservice.ProductType;
import com.mycompany.warehouseservice.ProductTypeMapper;
import com.mycompany.warehouseservice.api.model.ProductTypeDto;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = WarehouseService.class)
public class WarehouseServiceApplicationTests {
    @Autowired
    WarehouseProductApiDelegateImpl warehouseProductApiDelegate;
    @Autowired
    WarehouseUnreservedProductByProductTypeApiDelegateImpl warehouseUnreservedProductApiDelegate;
    @Autowired
    ProductTypeApiDelegateImpl productTypeApiDelegate;

    public WarehouseProduct initWP(Integer weight, String location, Boolean booked, ProductType product_id)
    {
        WarehouseProduct testWP = new WarehouseProduct();
        testWP.setWeight(weight);
        testWP.setLocation(location);
        testWP.setBooked(booked);
        testWP.setProductType(product_id);
        return testWP;
    }

    public ProductType initPT(String name)
    {
        ProductType testPT = new ProductType();
        testPT.setName(name);
        return testPT;
    }

    @ParameterizedTest
    @CsvSource({"Telefon,", "Notebook"})
    void addProductTypeTest(String name) {
        System.out.println("product type addition: " + name);
        ProductType testPT = initPT(name);
        ProductTypeDto productTypeDto = ProductTypeMapper.INSTANCE.mapTo(testPT);
        ResponseEntity response = productTypeApiDelegate.addProductType(productTypeDto);
        assertEquals(200, response.getStatusCode().value());
        System.out.println("product type addition works");
    }

    @ParameterizedTest
    @CsvSource({"Telefon", "Notebook"})
    void getAllProductTypeTest(String name) {
        System.out.println("product type get: " + name);
        ProductType testPT = initPT(name);
        ProductTypeDto productTypeDto = ProductTypeMapper.INSTANCE.mapTo(testPT);
        ResponseEntity response_added = productTypeApiDelegate.addProductType(productTypeDto);

        ResponseEntity<List<ProductTypeDto>> response = productTypeApiDelegate.productTypeGet();
        assertEquals(200, response.getStatusCode().value());
        System.out.println("product type get works");
    }

    @ParameterizedTest
    @CsvSource({"Telefon, 25, Praha, true", "Notebook, 25, Praha, false"})
    void addWarehouseProductTest(String name, Integer weight, String location, Boolean booked) {
        System.out.println("warehouse product addition: " + name);
        ProductType testPT = initPT(name);
        ProductTypeDto productTypeDto = ProductTypeMapper.INSTANCE.mapTo(testPT);
        ResponseEntity response_pt = productTypeApiDelegate.addProductType(productTypeDto);

        ResponseEntity<List<ProductTypeDto>> get_response_pt = productTypeApiDelegate.productTypeGet();

        List<ProductTypeDto> responsePTlist = get_response_pt.getBody();
        ProductType testPT_returned = ProductTypeMapper.INSTANCE.mapTo(responsePTlist.get(0));

        WarehouseProduct testWP = initWP(weight, location, booked, testPT_returned);

        WarehouseProductDto warehouseProductDto = WarehouseProductMapper.INSTANCE.mapTo(testWP);
        ResponseEntity response_end = warehouseProductApiDelegate.addWarehouseProduct(warehouseProductDto);
        assertEquals(200, response_end.getStatusCode().value());
        System.out.println("warehouse product addition works");
    }

    @ParameterizedTest
    @CsvSource({"Telefon, 25, Praha, true", "Notebook, 25, Praha, false"})
    void getAllWarehouseProductTest(String name, Integer weight, String location, Boolean booked) {
        System.out.println("get warehouse product: " + name);
        ProductType testPT = initPT(name);
        ProductTypeDto productTypeDto = ProductTypeMapper.INSTANCE.mapTo(testPT);
        ResponseEntity response_pt = productTypeApiDelegate.addProductType(productTypeDto);

        ResponseEntity<List<ProductTypeDto>> get_response_pt = productTypeApiDelegate.productTypeGet();

        List<ProductTypeDto> responsePTlist = get_response_pt.getBody();
        ProductType testPT_returned = ProductTypeMapper.INSTANCE.mapTo(responsePTlist.get(0));

        WarehouseProduct testWP = initWP(weight, location, booked, testPT_returned);

        WarehouseProductDto warehouseProductDto = WarehouseProductMapper.INSTANCE.mapTo(testWP);
        ResponseEntity response_add = warehouseProductApiDelegate.addWarehouseProduct(warehouseProductDto);
        System.out.println("added warehouse product: " + name);
        ResponseEntity<List<WarehouseProductDto>> response_end = warehouseProductApiDelegate.warehouseProductGet();

        assertEquals(200, response_end.getStatusCode().value());
        System.out.println("get warehouse product works");
    }

    @ParameterizedTest
    @CsvSource({"Telefon, 25, Praha, false", "Notebook, 25, Praha, false"})
    void putWarehouseProductTest(String name, Integer weight, String location, Boolean booked) {
        System.out.println("warehouse product put test: " + name);
        ProductType testPT = initPT(name);
        ProductTypeDto productTypeDto = ProductTypeMapper.INSTANCE.mapTo(testPT);
        ResponseEntity response_pt = productTypeApiDelegate.addProductType(productTypeDto);

        ResponseEntity<List<ProductTypeDto>> get_response_pt = productTypeApiDelegate.productTypeGet();

        List<ProductTypeDto> responsePTlist = get_response_pt.getBody();
        ProductType testPT_returned = ProductTypeMapper.INSTANCE.mapTo(responsePTlist.get(0));

        WarehouseProduct testWP = initWP(weight, location, booked, testPT_returned);

        WarehouseProductDto warehouseProductDto = WarehouseProductMapper.INSTANCE.mapTo(testWP);
        ResponseEntity response_end = warehouseProductApiDelegate.addWarehouseProduct(warehouseProductDto);

        ResponseEntity<List<WarehouseProductDto>> get_response_wp = warehouseProductApiDelegate.warehouseProductGet();
        List<WarehouseProductDto> responseWPlist = get_response_wp.getBody();
        WarehouseProduct toputWP = WarehouseProductMapper.INSTANCE.mapTo(responseWPlist.get(0));

        toputWP.setBooked(true);
        WarehouseProductDto toputWPdto = WarehouseProductMapper.INSTANCE.mapTo(toputWP);

        ResponseEntity put_response = warehouseProductApiDelegate.updateWarehouseProduct(toputWP.getId(), toputWPdto);

        assertEquals(200, put_response.getStatusCode().value());
        System.out.println("warehouse product put test works");
    }
}