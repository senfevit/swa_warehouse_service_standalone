DROP TABLE IF EXISTS warehouse_product_table;
DROP TABLE IF EXISTS product_type_table;

CREATE TABLE product_type_table(id SERIAL PRIMARY KEY, name VARCHAR(255));

--CREATE TABLE warehouse_product_table(id SERIAL PRIMARY KEY, weight integer, location VARCHAR(255), product_id INT REFERENCES product_type);
CREATE TABLE warehouse_product_table(
                                        id SERIAL PRIMARY KEY,
                                        weight integer,
                                        location VARCHAR(255),
                                        booked boolean,
                                        product_id INT REFERENCES product_type_table (id));

INSERT INTO product_type_table(name) VALUES('telefon');

INSERT INTO warehouse_product_table(weight, location, booked, product_id) VALUES(11,'Bratislava', TRUE, (SELECT id from product_type_table WHERE name='telefon'));
INSERT INTO warehouse_product_table(weight, location, booked, product_id) VALUES(22, 'Budapest', FALSE, (SELECT id from product_type_table WHERE name='telefon'));
