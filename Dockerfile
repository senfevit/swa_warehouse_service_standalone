FROM maven:latest
COPY settings.xml /usr/share/maven/conf/settings.xml
ADD . /projects/warehouse_service
WORKDIR /projects/warehouse_service
CMD ["mvn", "spring-boot:run"]